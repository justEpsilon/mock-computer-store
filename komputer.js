// ------------------------------------
// Variables, from HTML elements and additional
// ------------------------------------

    // Bank: 
let btnGetLoan = document.getElementById("btn-get-loan");
let btnRepayLoan = document.getElementById("btn-repay-loan");
let divBalance = document.getElementById("div-bank-balance");
let divOutstandingLoan = document.getElementById("div-outstanding-loan");
let pOutstandingLoan = document.getElementById("p-outstanding-loan");

    // Work: 
let btnBank = document.getElementById("btn-bank");
let btnWork = document.getElementById("btn-work");
let divPay = document.getElementById("div-work-pay");

    // Laptops: 
let btnBuy = document.getElementById("btn-buy");
let imgElement = document.getElementById("img-computer");
let priceElement = document.getElementById("comp-price");
let computersElement = document.getElementById("select-laptop");
let featuresElement = document.getElementById("features");
let featuresTitleElement = document.getElementById("features-title");
let computerDisplayElement = document.getElementById("computer-display");
let computerDisplayTitleElement = document.getElementById("computer-display-title");

// Additional variables: 
let bankBalance = 0;
let outstandingLoan = 0;
let payBalance = 0;

let computers = [];
let bigSpender = true; // this boolean is for tracking if you have bought a computer, before next loan. 
let countWork = 0; // keeps track of how many times you have pushed work button
let salary = 100; // money you earn while working 

const money = ' MONEYS';
const API_ROOT = "https://noroff-komputer-store-api.herokuapp.com/";

// Assigning values:
divBalance.innerText = String(bankBalance + money);
divPay.innerText = String(payBalance + money);
selectedIndex = 0;

// ------------------------------------
// Laptops: 
// ------------------------------------

// Loading dummy API, fetching computers from it

fetch(API_ROOT + "computers") // returns a promise. 
    .then(response => response.json()) // type of data
    .then(data => computers = data)  //loading values to computers[] array.
    .then(computers => addComputersToList(computers));

// adds all of the computers to drop down bar: 
const addComputersToList = (computers) => {
    computers.forEach(x => addComputer(x)); //x represent an idividual computer in computers collection . 
    //computersElement.innerText = computers[0].title; //adding a first price to the html, before we listen to event for pricing. < hidden div with computers not nessesary. 
}
// creates the computer element in the pulldown bar: 
const addComputer = (computer) => {
    const computerElement = document.createElement("option"); // can be any kind of element. 
    computerElement.value = computer.id; 
    computerElement.appendChild(document.createTextNode(computer.title)) // so it shows title instead of Id
    computersElement.appendChild(computerElement);
}

// Laptop select and display part: 

// select element:
const handleComputerMenuChange = e => {
    selectedIndex = e.target.selectedIndex - 1; // So we know which computer to purchase 
    featuresElement.innerText = '';
    if (selectedIndex < 0) {
        featuresTitleElement.hidden = true;
        computerDisplayElement.hidden = true;
    } else {
        featuresTitleElement.hidden = false;
        computerDisplayElement.hidden = false;
        const selectedComputer = computers[selectedIndex]; 
        computerDisplayTitleElement.innerText = `You have selected ${selectedComputer.title}`;
        priceElement.innerText = selectedComputer.price + money;
        selectedComputer.specs.forEach(spec => addSpecToList(spec));
        imgElement.setAttribute("src", API_ROOT + selectedComputer.image);
    }
}

const addSpecToList = spec => {
    const featuresItem = document.createElement("li");
    featuresItem.innerText = spec; 
    featuresElement.appendChild(featuresItem); //we are appendiong cart item to the cart element so it shows at HTML
}

// Purchasing a computer: 
const handleComputerPurchase = e => {
    let selectedComputer = computers[selectedIndex];
    let price = selectedComputer.price;
    let title = selectedComputer.title;
    if (bankBalance < price) {
        alert(`You don't have enough${money} for this! Go Work!`);
    } else {
        bankBalance -= price;
        divBalance.innerText = String(bankBalance + money);
        alert(`Thank you for your purchase of ${title}`);
        bigSpender = true; // so you can get a loan again!
    }

     
}

// Events: 

computersElement.addEventListener('change', handleComputerMenuChange);
btnBuy.addEventListener('click', handleComputerPurchase);

// ------------------------------------
// WORK part: (only events)
// ------------------------------------

btnWork.addEventListener('click', function() {
    console.log("Work button was clicked");

    payBalance = payBalance + salary;
    divPay.innerText = String(payBalance + money); 
    countWork += 1;
        if (countWork === 10) {
            salary = 1000;
            alert("Congradulations! You been working really hard, you get promoted to Komputer CEO, check out your new salary!!")

        }

})

//
// Conditionals a bit messy would prefer to swap them. 

btnBank.addEventListener('click', function() { 
        console.log(outstandingLoan);
    if (outstandingLoan != 0) {  //got a loan? must pay 10% of your pay back. 
        let loanFee = payBalance*0.1; 
        bankBalance = bankBalance + payBalance - loanFee;
        outstandingLoan = outstandingLoan - loanFee;
        if (outstandingLoan < 0) {  // if you earned more than your loan, rest of it goes to the bank balance. 
            bankBalance = bankBalance - outstandingLoan;
            outstandingLoan = 0;
        }

        if (outstandingLoan == 0) {
            btnRepayLoan.hidden = true;
            divOutstandingLoan.hidden = true;
            pOutstandingLoan.hidden = true;
            btnGetLoan.hidden = false;
        }
    } else { // No loan? congrads all the money goes to your bank account!
        bankBalance = bankBalance + payBalance
    }  
        payBalance = 0;
        divPay.innerText = String(payBalance + money);
        divBalance.innerText = String(bankBalance + money);
        divOutstandingLoan.innerText = String(outstandingLoan + money);
    
})
// ------------------------------------
// BANK part: 
// ------------------------------------
// Events: 

btnGetLoan.addEventListener('click', function() { 
   if (bigSpender) {
        let amount = Number(prompt(`How much${money} would you like to loan? `));
        if (amount !== amount || amount < 0) {
            console.error('amount must be a positive number');
            alert('amount must be a positive number')
        } else {
        getLoan(amount)
        divBalance.innerText = String(bankBalance + money);
        divOutstandingLoan.innerText = String(outstandingLoan + money);
        } 
        if (outstandingLoan !== 0){
                divOutstandingLoan.hidden = false;
                pOutstandingLoan.hidden = false;
                btnRepayLoan.hidden = false;
                btnGetLoan.hidden = true;
        }
           
    } else {
         alert('You must BUY before you loan, SPEND! Look at all the Komputers!');        
    }
}) 

btnRepayLoan.addEventListener('click', function(){
    console.log('repay loan button was clicked');
    
    if (outstandingLoan < payBalance){
        payBalance = payBalance - outstandingLoan;
        outstandingLoan = 0;
    } else {
        outstandingLoan = outstandingLoan - payBalance;
        payBalance = 0;
    }

    if (outstandingLoan == 0) {
        btnRepayLoan.hidden = true;
        divOutstandingLoan.hidden = true;
        pOutstandingLoan.hidden = true;
        btnGetLoan.hidden = false;
    } 

    divOutstandingLoan.innerText = String(outstandingLoan + money);
    divPay.innerText = String(payBalance + money);

})

function getLoan(amount) {
    if (amount > bankBalance*2){
        console.error('request is too large');
        alert(`amount is too large by ${amount.toFixed(2) - bankBalance*2} ${money}`);
    } else if (outstandingLoan != 0){
        console.error('Yo! more work more loan'); //this is unnessisery, since the button disapear after the loan have been issued.
        alert(`You already have a loan Darling!`);
    } else {
        outstandingLoan = amount;
        bankBalance = bankBalance + outstandingLoan;

        if (outstandingLoan!= 0) {
            bigSpender = false; //if I push cancel here, it will still make this false, therefore i need another conditional. 
        }
    }
}



